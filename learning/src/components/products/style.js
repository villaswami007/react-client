import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  root: {
    maxWidth: "100%", 

  },
  cardbackground: {
    backgroundColor: "#C7DFC1",
  },
  media: {
    height: 0,
    width:'100%',
    paddingTop: '76.25%', // 16:9
  },
  cardActions: {
    display: "flex",
    justifyContent: "flex-end",
  },
  cardContent: {
    display: "flex",
    justifyContent: "space-between",
  },
}));
