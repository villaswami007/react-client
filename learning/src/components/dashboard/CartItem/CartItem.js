import React from 'react';
import { Typography, Button, Card, CardActions, CardContent, CardMedia } from '@material-ui/core';

import useStyles from './Style';

const CartItem = ({ item,removecart,handlequantity}) => {
  const handleUpdateCartQty = (lineItemId, newQuantity) => handlequantity(lineItemId, newQuantity);

  const handleRemoveFromCart = (lineItemId) => removecart(lineItemId);
  const classes = useStyles();
  console.log(item.image.url)
  return (
    <Card className="cart-item">
     <img src={item.image.url} alt='image' className={classes.media}/>
      <CardContent className={classes.cardContent}>
        <Typography variant="h6">{item.name}</Typography>
      </CardContent>
      <CardActions className={classes.cartActions}>
        <Button variant="contained" type="button" color="secondary" onClick={()=>handleRemoveFromCart(item.id)}>Unenroll</Button>
      </CardActions>
    </Card>
  );
};

export default CartItem;