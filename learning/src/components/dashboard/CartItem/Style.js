import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  cardContent: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  media:{
    width:'100%'
  },
  cartActions: {
    display:'flex',
    justifyContent: 'flex-end',
  },
  buttons: {
    display: 'flex',
    alignItems: 'center',
  },
}));