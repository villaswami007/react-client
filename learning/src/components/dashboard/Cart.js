import React from 'react';
import { Container, Typography, Button, Grid,AppBar,Toolbar } from '@material-ui/core';
import { Link } from 'react-router-dom';

import CartItem from './CartItem/CartItem';
import useStyles from './Style';

const Cart = ({ cart1, handlequantity, removecart, emptycart }) => {
  const classes = useStyles();

  const handleEmptyCart = () => emptycart();

  const renderEmptyCart = () => (
    <Typography variant="h3" style={{ fontFamily: 'Rosarivo'}}>Your dashboard is Empty,
      <Link className={classes.link} to="/"><Button color='secondary' variant='contained' style={{ fontFamily: 'Rosarivo'}}>Return to All COurses</Button></Link>
    </Typography>
  );

  if (!cart1.line_items) return 'Loading';

  const renderCart = () => (
    <>
    
      <Grid container spacing={3}>
        {cart1.line_items.map((lineItem) => (
          <Grid item xs={12} sm={4} key={lineItem.id}>
            <CartItem item={lineItem} handlequantity={handlequantity} removecart={removecart} />
          </Grid>
        ))}
      </Grid>
    </>
  );

  return (
    <Container>
      <div className={classes.toolbar} />
      <AppBar
        style={{ display: "flex", justifyContent: "space-between" }}
        color="inherit"
      >
        <Toolbar style={{ display: "flex", justifyContent: "space-between" }}>
          <img
            className="img-logo"
            src="https://crystaldelta.com/wp-content/themes/CrystalDelta/img/logo/crystal-delta_b.png"
            alt=""
          />
          <Button variant="outlined" color="primary">
            Logout
          </Button>
        </Toolbar>
      </AppBar>
      <div style={{ margin: "5vh" }}></div>
      <Link className="link" to="/">
        All Courses
      </Link>
      <Link className="link" to="/dashboard">
        My Dashboard
      </Link>
      { !cart1.line_items.length ? renderEmptyCart() : renderCart() }
    </Container>
  );
};

export default Cart;