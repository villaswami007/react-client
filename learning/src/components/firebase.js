import React from 'react';
import {initializeApp} from 'firebase/app';
import { getAuth} from "firebase/auth";
const firebaseConfig = {
    apiKey: "AIzaSyBmHAZ2oDPJTT97lIzgTWxp0Q7yGARTj3k",
    authDomain: "e-learning-app-d73b4.firebaseapp.com",
    projectId: "e-learning-app-d73b4",
    storageBucket: "e-learning-app-d73b4.appspot.com",
    messagingSenderId: "974389573188",
    appId: "1:974389573188:web:8d9a3a269fa2c3449fae11"
  };
const app=initializeApp(firebaseConfig);
export const auth = getAuth(app);